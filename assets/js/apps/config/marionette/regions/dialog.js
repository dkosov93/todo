Marionette.Region.Dialog = Marionette.Region.extend({
  onShow: function(view){
    this.listenTo(view, "dialog:empty", this.emptyDialog);

    var self = this;
    this.$el.dialog({
      modal: true,
      title: view.title,
      width: "auto",
      close: function(e, ui){
        self.emptyDialog();
      }
    });
  },
  
  emptyDialog: function(){
    this.stopListening();
    this.empty();
    this.$el.dialog("destroy");
  }
});
